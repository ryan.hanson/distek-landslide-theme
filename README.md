# DISTek Landslide Theme

A Landslide Slideshow Theme for DISTek

See https://github.com/adamzap/landslide for more information


## Changes from Default Slide Deck

So far all that has changed was the css to use the DISTek background and
title colors.
